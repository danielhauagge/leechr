# ====================================================================== #
# Copyright (C) 2013 Daniel Hauagge                                      #
#                                                                        #
# Permission is hereby granted, free  of charge, to any person obtaining #
# a  copy  of this  software  and  associated  documentation files  (the #
# "Software"), to  deal in  the Software without  restriction, including #
# without limitation  the rights to  use, copy, modify,  merge, publish, #
# distribute,  sublicense, and/or sell  copies of  the Software,  and to #
# permit persons to whom the Software  is furnished to do so, subject to #
# the following conditions:                                              #
#                                                                        #
# The  above  copyright  notice  and  this permission  notice  shall  be #
# included in all copies or substantial portions of the Software.        #
#                                                                        #
# THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND, #
# EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF #
# MERCHANTABILITY,    FITNESS    FOR    A   PARTICULAR    PURPOSE    AND #
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE #
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION #
# OF CONTRACT, TORT OR OTHERWISE,  ARISING FROM, OUT OF OR IN CONNECTION #
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.        #
# ====================================================================== #

from os import path
import sys
from bs4 import BeautifulSoup
from bs4.element import Tag
from urllib import urlretrieve, ContentTooShortError, urlencode
import urllib2
from urllib2 import urlopen, HTTPError, URLError, Request
import httplib
from threading import Thread, Lock, Semaphore, Condition
from optparse import OptionParser
from datetime import datetime, date, timedelta
import json
from copy import copy
import logging
import calendar
from copy import deepcopy

# ====================================================================== #
# Exceptions                                                             #
# ====================================================================== #
class LeechrException(Exception):
    def __init__(self, code, message):
        Exception.__init__(self, message)
        self.code = code

# ====================================================================== #
# Utils                                                                  #
# ====================================================================== #
# def sanitize_field_name(field_name):

#     replacements = [(' ', '_'), ('-', ''), ('(', ''), (')', ''), (':','_')]
#     for rep in replacements:
#         field_name = field_name.replace(rep[0], rep[1])

#     return field_name.strip().lower()

def load_image_list(fname):
    """ Loads all images listed in a file. Only keeps first word of each
        line (so that it can take the list files wht focal length that
        bundler uses).
        @param fname: the list filename
    """
    data = open(fname).read().strip().split('\n')
    data = [d.split()[0].strip() for d in data]
    data = [d for d in data if len(d) != 0]
    return data

def date2timestamp(d):
    """Converts a string in the leechr specified format to a unix timestamp"""

    d = datetime.strptime(d, '%Y:%m:%d')

    return datetime.strftime(d, '%Y-%m-%d %H:%M:%S')

# ====================================================================== #
# Flickr Related                                                         #
# ====================================================================== #
def split_userid_imageid(fname):
    fname = path.basename(path.splitext(fname)[0])

    no_userid = False
    if len(fname) > 2 and fname[-2:] in ['_o', '_m']:
        fname = fname[:-2]
        no_userid = True
    if len(fname) > 4 and fname[-4:] == '_o_d':
        fname = fname[:-4]
        no_userid = True

    if no_userid:
        idx = fname.find('_')
        imageid = fname[:idx]
        userid = find_userid_for_imageid(imageid)
    else:
        idx = fname.rfind('_')
        userid = fname[:idx]
        imageid = fname[idx + 1:]

    return userid, imageid

def find_userid_for_imageid(imageid):
    urlfmt = 'https://www.flickr.com/photo_zoom.gne?id=%s'

    try:
        url = urlfmt%(imageid)
        html_doc = urlopen(url)
    except HTTPError as err:
        #log_msg('Got an Http error when trying to download %s'%(url))
        return ''

    # When queried for the above url Flickr redirects us to a
    # url that contains the userid
    toks = html_doc.geturl().split('/')
    if len(toks) >= 5:
        return toks[4]

def get_neighboring_imgs_for_img(userid, imageid, n_neighbors = 3):
    """ Finds the url for the images that came before and after a given
        image in the user photo stream.
        @param userid: a string containing the flickr user id
        @param imageid: a string containing the flickr image id
        @param n_neighbors: how many images to include before (if negative) or after
            (if positive) the current one, or before and after (if tuple with two integers).
        @returns neighbors: a list of the photo ids neighboring the current image. The current
            image is included in the results.
    """

    if not isinstance(n_neighbors, int):
        before = get_neighboring_imgs_for_img(userid, imageid, -abs(n_neighbors[0]))[:-1]
        after = get_neighboring_imgs_for_img(userid, imageid, n_neighbors[1])[1:]
        return before + [(userid, imageid)] + after


    direction = 'before' if n_neighbors < 0 else 'after'
    n_neighbors = abs(n_neighbors)

    html_doc = grab_flickr_page_for_image(userid, imageid)

    soup = BeautifulSoup(html_doc)

    # Strategy here is to find the links by following the thumbnails
    # that appear under "This photo belongs to"
    tag = soup.find(id = 'context-photos-list-stream-')
    if tag != None:
        links = [t.get('href') for t in tag.find_all(class_ = 'context-thumb-link') if t != None]

        # Grab image ids for all links
        imageids = [l.split('/')[3] for l in links if l != None]

        curr_img_pos = imageids.index(imageid)

        before = imageids[:curr_img_pos]
        after = imageids[curr_img_pos + 1:]

        before = [(userid, imid) for imid in before]
        after = [(userid, imid) for imid in after]

        if direction == 'before':

            if len(before) < n_neighbors and len(before) != 0:
                before += get_neighboring_imgs_for_img(userid, before[0][1], n_neighbors = -(n_neighbors - len(before)))[:-1]
            else:
                before = before[:n_neighbors]

            return before + [(userid, imageid)]

        else:
            if len(after) < n_neighbors and len(after) != 0:
                after += get_neighboring_imgs_for_img(userid, after[-1][1], n_neighbors = n_neighbors - len(after))[1:]
            else:
                after = after[:n_neighbors]

            return [(userid, imageid)] + after
    else:
        return []


def get_exif(photoid, options):
    params = {
        'method'            :   'flickr.photos.getExif',
        'api_key'           :   options.api_key,
        'format'            :   'json',
        'nojsoncallback'    :   1,
        'photo_id'          :   photoid,
    }
    r = urlopen('https://api.flickr.com/services/rest/?%s'%urlencode(params))
    data = r.read()
    response = json.loads(data)
    if response['stat'] == 'fail':
        # Some errors should be hidden
        # I think the only case we don't want to handle is permission denied
        hide_error = {
            1: False, # photo not found
            2: True, # permission denied
            100: False, # invalid api key
            105: False, # service currently unavailable
            111: False, # format "xxx" not found
            112: False, # method "xxx" not found
            114: False, # invalid soap envelope
            115: False, # invalid xml-rpc method call
            116: False, # bad url found
        }[response['code']]

        if hide_error:
            return {}, ''

        raise LeechrException(response['code'], response['message'])

    response = { str(tag['tag']): tag['raw']['_content'] for tag in response['photo']['exif'] }

    return response

def __flickr_query(query, options):
    # Reference: http://www.flickr.com/services/api/flickr.photos.search.html
    page = 1
    nPages = 1

    while page <= nPages:
        params = {
            'method'            :   'flickr.photos.search',
            'api_key'           :   options.api_key,
            'text'              :   query,
            'format'            :   'json',
            'nojsoncallback'    :   1,
            'page'              :   page,
            'per_page'          :   500,
            'extras'            :   'description,date_taken,owner_name,geo_tags',
            # 'privacy_filter'    :   1
        }
        if options.after_date != None :
            params['min_taken_date'] = date2timestamp(options.after_date)
        if options.before_date != None :
            params['max_taken_date'] = date2timestamp(options.before_date)

        if options.only_creative_commons:
            # Reference: http://www.flickr.com/services/api/flickr.photos.licenses.getInfo.html
            params['license'] = 1

        url = 'https://api.flickr.com/services/rest/?%s'%urlencode(params)
        r = urlopen(url)
        data = r.read()
        response = json.loads(data)

        if response['stat'] == 'fail':
            raise LeechrException(response['code'], response['message'])

        nPages = response['photos']['pages']

        if response['photos']['page'] != page:
            logging.warn('Page number in response %d differs from our page number %d', response['photos']['page'], page)

        yield response

        page += 1

def flickr_query(query, options_, only_first_page = False):

    options = deepcopy(options_)

    def date2str(d): return datetime.strftime(d, '%Y:%m:%d')
    def str2date(s): return datetime.strptime(s, '%Y:%m:%d')

    # Start date
    if options.after_date != None:
        start_date = str2date(options.after_date)
    else:
        start_date = str2date('2000:1:1')

    # End date
    if options.before_date != None:
        end_date = str2date(options.before_date)
    else:
        end_date = datetime.today()

    # Iterate over date ranges
    delta_month = timedelta(days = 30)
    prev_date = start_date
    curr_date = start_date + delta_month

    while prev_date < end_date:
        print curr_date

        options.after_date = date2str(prev_date)
        options.before_date = date2str(min(end_date, curr_date))
        # options.before_date = date2str(curr_date)

        # print options.after_date, options.before_date
        for response in __flickr_query(query, options):
            print '# photos: %d'%(int(response['photos']['total']))
            # print response['photos'].keys()
            # print response['photos']['photo']
            # sys.exit(1)
            yield response

            if only_first_page: break

        prev_date = curr_date
        curr_date = curr_date + delta_month

def get_image_file_entry(photoid, options):
    """Returns the file entry for the largest image for the given photoid
    """
    params = {
        'method'            :   'flickr.photos.getSizes',
        'api_key'           :   options.api_key,
        'format'            :   'json',
        'nojsoncallback'    :   1,
        'photo_id'          :   photoid,
    }
    r = urlopen('https://api.flickr.com/services/rest/?%s'%urlencode(params))
    data = r.read()
    response = json.loads(data)

    if response['stat'] == 'fail':
        raise LeechrException(response['code'], response['message'])

    max_size = -1
    entry_largest = ''
    for entry in response['sizes']['size']:
        if entry['media'] != 'photo': continue

        width = int(entry['width'])
        height = int(entry['height'])

        size =  width * height
        if size > max_size:
            max_size = size
            entry_largest = entry

    return entry_largest

def get_image(image_url):
    r = urllib2.urlopen(image_url)
    data = r.read()
    return data

def get_nhits_for_query(query, options):
    tot = 0
    for response in flickr_query(query, options, only_first_page = True):
        tot += int(response['photos']['total'])

    return tot

def search(query, options):
     for response in flickr_query(query, options):

        print 'resp len %d'%(len(response['photos']['photo']))

        print 'page', response['photos']['page']
        print 'pages', response['photos']['pages']
        print 'total', response['photos']['total']

        for photo in response['photos']['photo']:
            photo_obj = {
                'photoid'               :   photo['id'],
                'userid'                :   photo['owner'],
                'username'              :   photo['ownername'],
                'title'                 :   photo['title'],
                'datetaken'             :   photo['datetaken'],
                'description'           :   photo['description']['_content'],
                'datetakengranularity'  :   photo['datetakengranularity'],
            }

            yield photo_obj

# ====================================================================== #
# Threads                                                                #
# ====================================================================== #
class ImageDownloaderWorker(Thread):
    def __init__(self, manager, job_id, job, options):
        Thread.__init__(self)
        # Setting the thread to be a daemon is necessary if we want the
        # application to exit on ctrl+c (Ref: http://goo.gl/nO3MWc)
        self.daemon = True
        self.job_id = job_id
        self.manager = manager
        self.img_url = job[0]
        self.img_metadata = job[1]
        self.output_dir = options['output_dir']
        self.save_metadata = options['save_metadata']

    def run(self):
        #img_fname = path.basename(self.img_url)
        img_fname = '%s_%s.jpg'%(self.img_metadata['userid'], self.img_metadata['photoid'])

        output_fname = path.join(self.output_dir, img_fname)
        if path.splitext(img_fname)[1] == '':
            output_fname += '.jpg'

        metadata_fname = path.join(self.output_dir, path.splitext(img_fname)[0] + '.json')

        # Don't download the image if we already have the file
        if path.exists(output_fname):
            self.manager.handin_results(self.job_id, 'ok', 'Skipping %s (already downloaded)'%(path.basename(output_fname)))
            return

        # Download the content
        try:
            urlretrieve(self.img_url, output_fname)

            if self.save_metadata:
                with open(metadata_fname, 'w') as f:
                    json.dump(self.img_metadata, f)

        except ContentTooShortError as err:
            self.manager.handin_results(self.job_id, 'failed', 'Truncated file')
        except IOError as err:
            self.manager.handin_results(self.job_id, 'failed', 'IO error while downloading')

        # Report back to manager that we've finished
        self.manager.handin_results(self.job_id, 'ok', 'Downloaded %s'%(img_fname))


class ThreadManager:
    def __init__(self, thread_classname, n_threads, worker_options = {}):
        self.results_lock = Lock()
        self.workers = Semaphore(n_threads)
        self.worker_options = worker_options
        self.thread_classname = thread_classname

    def handin_results(self, job_id, status, msg):
        with self.results_lock:
            self.n_done_jobs += 1

            #log_msg = '[%8d/%d][JobID: %4d] %s'%(self.n_done_jobs, self.n_jobs, job_id, msg)
            log_msg = '[%8d/%d] %s'%(self.n_done_jobs, self.n_jobs, msg)

            if status == 'ok':
                logging.info( log_msg )
            elif status == 'error':
                logging.warn( log_msg )
            else:
                logging.error( log_msg )

        self.workers.release()

    def __call__(self, jobs, n_jobs):

        self.n_done_jobs = 0
        self.n_jobs = n_jobs

        all_workers = []
        for job_id, job in enumerate(jobs()):
            self.workers.acquire()
            worker = globals()[self.thread_classname](self, job_id, job, self.worker_options)
            worker.start()
            all_workers.append(worker)

        # Wait for everyone to finish
        for thread in all_workers:
            thread.join()
